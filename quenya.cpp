#include <cstdint>
#include <cstring>
#include <cstdio>
#include <vector>
#include <unordered_map>

namespace elf {
    using namespace std;

    using u64 = uint64_t;
    using i64 = int64_t;
    using u32 = uint32_t;
    using i32 = int32_t;
    using u8 = uint8_t;
    using i8 = int8_t;
    using addr = uint64_t;
    using off = uint64_t;
    using half = uint16_t;
    using word = uint32_t;
    using sword = int32_t;
    using xword = uint64_t;
    using sxword = int64_t;
    using chr = uint8_t;

    const int EHDR_SIZE = 0x40;

    chr operator "" _char(unsigned long long i) {
        return i;
    }

    chr operator "" _c(unsigned long long i) {
        return i;
    }

    half operator "" _half(unsigned long long i) {
        return i;
    }

    half operator "" _h(unsigned long long i) {
        return i;
    }

    word operator "" _word(unsigned long long i) {
        return i;
    }

    word operator "" _w(unsigned long long i) {
        return i;
    }

    xword operator "" _xword(unsigned long long i) {
        return i;
    }

    xword operator "" _x(unsigned long long i) {
        return i;
    }

    addr operator "" _addr(unsigned long long i) {
        return i;
    }

    off operator "" _off(unsigned long long i) {
        return i;
    }

    u64 elf64_hash(const u8* name) {
        u64 h = 0, g;
        while (*name) {
            h = (h << 4) + *name++;
            if (g = h & 0xf0000000)
            h ^= g >> 24;
            h &= 0x0fffffff;
        }
        return h;
    }

    enum endian {
        LITTLE = 1, 
        BIG = 2
    };

    enum objtype {
        NO_TYPE = 0, 
        OBJECT = 1, 
        EXECUTABLE = 2, 
        SHARED_OBJECT = 3, 
        CORE = 4
    };

    enum osabi {
        SYSTEMV_ABI = 0x00, 
        HPUX_ABI = 0x01, 
        NETBSD_ABI = 0x02,
        LINUX_ABI = 0x03,
        GNUHURD_ABI = 0x04,
        SOLARIS_ABI = 0x06,
        AIX_ABI = 0x07,
        IRIX_ABI = 0x08,
        FREEBSD_ABI = 0x09,
        TRU64_ABI = 0x0A,
        OPENBSD_ABI = 0x0C,
        OPENVMS_ABI = 0x0D,
        NONSTOP_ABI = 0x0E,
        AROS_ABI = 0x0F,
        FENIX_ABI = 0x10,
        STANDALONE = 0xFF
    };

    enum arch {
        NO_ARCH = 0x00,
        SPARC = 0x02,
        X86 = 0x03,
        MIPS = 0x08,
        POWERPC = 0x14,
        S390 = 0x16,
        ARM = 0x28,
        SUPERH = 0x2A,
        IA64 = 0x32,
        X86_64 = 0x3E,
        AARCH64 = 0xB7,
        RISCV = 0xF3
    };

    class string {
        u8* data;
        u8 buf[16];
        u32 length, capacity;

        void grow() {
            capacity *= 2;
            u8* newdata = new u8[capacity];
            memcpy(newdata, data, length);
            newdata[length] = '\0';
            if (data != buf) delete[] data;
            data = newdata;
        }

    public:
        string(): length(0), capacity(16), data(buf) {
            *data = '\0';
        }

        ~string() {
            if (data != buf) delete[] data;
        }

        string(const string& other): length(other.length), capacity(other.capacity) {
            if (capacity > 16) data = new u8[capacity];
            else data = buf;
            memcpy(data, other.data, length);
            data[length] = '\0';
        }

        string(const char* str): string() {
            operator<<(str);
        }

        string& operator=(const string& other) {
            if (this != &other) {
                if (data != buf) delete[] data;
                length = other.length, capacity = other.capacity;
                if (capacity > 16) data = new u8[capacity];
                else data = buf;
                memcpy(data, other.data, length);
                data[length] = '\0';
            }
            return *this;
        }

        string& operator=(const char* str) {
            return operator=(string(str));
        }

        template<typename T>
        string& operator<<(const T& t) {
            while (length + 1 + sizeof(T) >= capacity) grow();
            *(T*)(data + length) = t;
            length += sizeof(T);
            data[length] = '\0';
            return *this;
        }
        
        string& operator<<(const uint8_t* s) {
            return operator<<((const char*)s);
        }

        string& operator<<(const char* s) {
            u64 len = strlen(s);
            while (length + 1 + len >= capacity) grow();
            memcpy(data + length, s, len);
            length += len;
            data[length] = '\0';
            return *this;
        }

        string& operator<<(const string& s) {
            u64 len = s.size();
            while (length + 1 + len >= capacity) grow();
            memcpy(data + length, s.data, len);
            length += len;
            data[length] = '\0';
            return *this;
        }

        u32 size() const {
            return length;
        }

        void write(FILE* f) {
            for (u32 i = 0; i < length; i ++) {
                fputc(data[i], f);
            }
        }

        const uint8_t* raw() const {
            return data;
        }

        bool operator==(const string& other) const {
            return length == other.length && !memcmp(data, other.data, length);
        }
    };
}

namespace std {
    template<>
    struct hash<elf::string> {
        size_t operator()(const elf::string& s) const {
            return elf::elf64_hash(s.raw());
        }
    };
}

namespace elf {
    enum section_type {
        SEC_NULL = 0,
        SEC_PROG = 1,
        SEC_SYMTAB = 2,
        SEC_STRTAB = 3,
        SEC_RELA = 4,
        SEC_HASH = 5,
        SEC_DYNAMIC = 6,
        SEC_NOTE = 7,
        SEC_NOBITS = 8,
        SEC_REL = 9,
        SEC_SHLIB = 10,
        SEC_DYNSYM = 11
    };

    struct section_header {
        word name_index;
        section_type type;
        xword flags;
        addr vma;
        off offset;
        xword size;
        word link;
        word info;
        xword align;
        xword entsize;

        section_header(): 
            name_index(0), type(SEC_NULL), flags(0),
            vma(0), offset(0), size(0), link(0), info(0),
            align(8), entsize(0) {
            //
        }
    };

    enum symbol_type {
        UNTYPED_SYMBOL = 0,
        DATA_SYMBOL = 1,
        FUNCTION_SYMBOL = 2,
        SECTION_SYMBOL = 3,
        FILE_SYMBOL = 4
    };

    struct symbol {
        string name;
        symbol_type type;
        bool local;
        off offset;
        off name_index;
        half section_index;
    public:
        symbol(): type(UNTYPED_SYMBOL), local(false),
            offset(0), name_index(0), section_index(0) {
            //
        }

        symbol(const string& name_in, symbol_type type_in, 
            bool local_in = false, off offset_in = 0):
            name(name_in), type(type_in), local(local_in),
            offset(offset_in), name_index(0), section_index(0) {
            //
        }
    };

    struct section {
        string name;
        string data;
        vector<symbol> symbols;
        bool executable, writable, initialized;

        section(): executable(false), writable(false), initialized(false) {
            //
        }
    };

    class object {
        string data;
        vector<section_header> sht;
        vector<string> sections;
        unordered_map<string, symbol> symbols;
        string shstrtab, strtab, symtab;

        arch architecture;
        endian endianness;
        objtype type;
        osabi os;
        addr entrypoint;

        void use_default_ctx() {
            type = OBJECT;
            os = SYSTEMV_ABI;
            entrypoint = 0;

            // endianness test
            int32_t i = 1;
            endianness = (*(int8_t*)&i) == 1 ? LITTLE : BIG;

            // architecture test
            #if defined(_M_X64) || defined(__x86_64__)
            architecture = X86_64;
            #elif defined(_M_ARM) || defined(__arm__)
            architecture = ARM;
            #elif defined(_M_ARM64) || defined(__aarch64__)
            architecture = AARCH64;
            #elif defined(_M_IX86) || defined(__i386__)
            architecture = X86;
            #elif defined(_M_IA64) || defined(__ia64__)
            architecture = IA64;
            #elif defined(__mips__) || defined(__mips)
            architecture = MIPS;
            #elif defined(_M_PPC) || defined(__powerpc__) || defined(__ppc__)
            architecture = POWERPC;
            #elif defined(__sparc__) || defined(__sparc)
            architecture = SPARC;
            #elif defined(__superh__)
            architecture = SUPERH;
            #endif

            section_header nullhdr;
            memset(&nullhdr, 0, sizeof(section_header));
            sht.push_back(nullhdr);
            string nullsection;
            sections.push_back(nullsection);

            strtab << '\0';
            shstrtab << '\0';
        }

    public:
        object() {
            use_default_ctx();
        }

        object& set_type(objtype t) {
            type = t;
        }

        object& set_machine(arch a) {
            architecture = a;
        }

        object& set_os(osabi o) {
            os = o;
        }

        object& add_section(section& s) {
            section_header shdr;
            shdr.offset = sht.back().offset + sht.back().size;
            while (shdr.offset % 8 != 0) shdr.offset ++;
            shdr.flags |= s.writable ? 0x1 : 0x0;
            shdr.flags |= 0x2;
            shdr.flags |= s.executable ? 0x4 : 0x0;

            shdr.name_index = shstrtab.size();
            shstrtab << s.name << '\0';

            shdr.type = s.initialized ? SEC_PROG : SEC_NOBITS;

            shdr.size = s.data.size();
            for (const symbol& sym : s.symbols) {
                symbols[sym.name] = sym;
                symbols[sym.name].name_index = strtab.size();
                symbols[sym.name].section_index = sht.size();
                strtab << sym.name << '\0';
            }
            sht.push_back(shdr);
            sections.push_back(s.data);

            return *this;
        }

        object& finalize_sections() {
            section_header shstrtab_hdr, strtab_hdr, symtab_hdr;
            strtab_hdr.offset = sht.back().offset + sht.back().size;
            while (strtab_hdr.offset % 8 != 0) strtab_hdr.offset ++;
            strtab_hdr.name_index = shstrtab.size();
            shstrtab << ".strtab" << '\0';
            strtab_hdr.type = SEC_STRTAB;
            strtab_hdr.size = strtab.size();
            sht.push_back(strtab_hdr);
            sections.push_back(strtab);

            symtab_hdr.offset = sht.back().offset + sht.back().size;
            while (symtab_hdr.offset % 8 != 0) symtab_hdr.offset ++;
            symtab_hdr.name_index = shstrtab.size();
            symtab_hdr.link = sht.size() - 1;   // link to string table.
            shstrtab << ".symtab" << '\0';
            symtab_hdr.type = SEC_SYMTAB;
            symtab << 0_xword << 0_xword << 0_xword; // empty first symbol.
            for (auto &p : symbols) {
                symtab << (word)p.second.name_index;
                symtab << (chr)((p.second.local ? 0b00000000 : 0b00010000) | p.second.type);
                symtab << 0_c;                  // symbol other (reserved).
                symtab << p.second.section_index;
                symtab << p.second.offset;
                symtab << 0_xword;              // symbol size (assume unknown/0).
            }
            symtab_hdr.entsize = 0x18;
            symtab_hdr.size = symtab.size();
            sht.push_back(symtab_hdr);
            sections.push_back(symtab);

            shstrtab_hdr.offset = sht.back().offset + sht.back().size;
            while (shstrtab_hdr.offset % 8 != 0) shstrtab_hdr.offset ++;
            shstrtab_hdr.flags = 0;

            shstrtab_hdr.name_index = shstrtab.size();
            shstrtab << ".shstrtab" << '\0';

            shstrtab_hdr.type = SEC_STRTAB;
            shstrtab_hdr.size = shstrtab.size();
            sht.push_back(shstrtab_hdr);
            sections.push_back(shstrtab);

            for (section_header& shdr : sht) {
                if (shdr.type != SEC_NULL) {
                    shdr.offset += EHDR_SIZE + sizeof(section_header) * sht.size();
                }
            }
            return *this;
        }

        void serialize_ident() {
            data << 0x7F_c << "ELF";
            data << 2_c;                   // 64-bit object class.
            data << (chr)endianness;       // endianness.
            data << 1_c;                   // ELF version (current).
            data << (chr)os;               // OS/ABI.
            data << 0_c;                   // ABI version (empty).
            for (int i = 0; i < 7; i ++) {
                data << 0_c;               // padding.
            }
        }

        void serialize_prefix() {
            serialize_ident();
            data << (half)type;             // object type.
            data << (half)architecture;     // machine.
            data << 1_w;                    // ELF version (current).
            data << entrypoint;             // entry point address.
            
            data << 0_off;                  // program header offset.
            data << 0x40_off;               // section header offset (64 bytes).
            data << 0_w;                    // processor-specific flags.
            data << 0x40_h;                 // ELF header size (64 bytes).

            data << 0_h;                    // size of program header entry.
            data << 0_h;                    // number of program header entries.
            data << half(sizeof(section_header));   // size of section header entry.
            data << half(sht.size());               // number of section header entries.
            data << half(sht.size() - 1);           // shstrtab header index.
        }

        void serialize_shdr(const section_header& shdr) {
            data << shdr.name_index;
            data << (word)shdr.type;
            data << shdr.flags;
            data << shdr.vma;
            data << shdr.offset;
            data << shdr.size;
            data << shdr.link;
            data << shdr.info;
            data << shdr.align;
            data << shdr.entsize;
        }

        void serialize_sections() {
            for (const section_header& shdr : sht) {
                serialize_shdr(shdr);
            }

            for (const string& s : sections) {
                data << s;
                for (u32 i = s.size(); i % 8 != 0; i ++) data << '\0';
            }
        }

        object& serialize() {
            serialize_prefix();
            serialize_sections();
            return *this;
        }

        void write(FILE* f) {
            data.write(f);
        }

        void write(const char* path) {
            FILE* f = fopen(path, "w");
            write(f);
        }
    };
}

using namespace elf;

int main(int argc, char** argv) {
    object o;
    o.set_type(OBJECT);

    section text;
    text.name = ".text";
    text.data << 0x00003CB8 << 0x0007BF00 << 0x050F0000;
    text.executable = true, text.initialized = true;
    text.symbols.push_back({ "_start", FUNCTION_SYMBOL });

    o.add_section(text).finalize_sections().serialize().write(argv[1]);
    return 0;
}