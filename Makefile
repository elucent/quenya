.PHONY: clean

quenya: quenya.cpp
	g++ -std=c++11 quenya.cpp -o quenya

clean:
	rm quenya *.o *.exe